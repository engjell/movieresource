from cslist.views import cslist_context_data
from allauth.account.views import SignupView
from django.views import generic

class HomeMixin(object):
  template_name = 'home.html'

  def get_context_data(self, **kwargs):
    context = super(HomeMixin, self).get_context_data(**kwargs)
    cslist_context_data(context, self.request)
    return context

class HomePage(HomeMixin, SignupView):
  pass

class HomeList(HomeMixin, generic.TemplateView):
  pass

from django.core.mail import send_mail
from django.http import HttpResponse

def email(request):
  result = "Success" if send_mail("Subject", "Message body", 'engjelldfc@gmail.com', [ 'jason.rukman@gmail.com' ]) == 1 else "Failed"
  return HttpResponse(result)