from django.db import models
from django.contrib.auth.models import User


class CSResource(models.Model):
  class Meta:
   verbose_name = "Movie Resource"
  name = models.CharField(max_length=40)
  url = models.URLField()
  description = models.CharField(max_length = 400, blank=True)
  grade_levels = models.CommaSeparatedIntegerField(max_length = len("K,1,2,3,4,5,6,7,8,9,10,11,12"), null=True, blank=True)
  rating_sum = models.IntegerField(default=0)
  rating_count = models.IntegerField(default=0)
  user = models.ForeignKey(User)
  promotional_video_url = models.URLField(null=True, blank=True)
  created_time = models.DateTimeField(auto_now_add=True)
  last_modified_time = models.DateTimeField(auto_now=True)

  def languages(self):
    return ",".join(str(language) for language in self.language_set.all())

  def price_tiers(self):
    return ",".join(str(price_tier) for price_tier in self.pricetier_set.all())

  def platforms(self):
    return ",".join(str(platform) for platform in self.platform_set.all())

  def rating(self):
    return int(round(self.rating_float()))

  def rating_float(self):
    return self.rating_sum / float(self.rating_count)

  def __unicode__(self):
    return "{0} [{1}]".format(self.name, self.url)

  def add_review(self, rating, user):
    review = Review(rating=rating, user=user, cs_resource=self)
    review.save()

    self.rating_count +=  1
    self.rating_sum += rating
    self.save()

    return review

class ScreenShot(models.Model):
  cs_resource = models.ForeignKey(CSResource)
  image = models.ImageField()
  description = models.CharField(max_length = 40, null=True, blank=True)

class PriceTier(models.Model):
  cs_resource = models.ForeignKey(CSResource)
  name = models.CharField(max_length=40)
  trial_days = models.IntegerField(null=True, blank=True)
  cost = models.FloatField(null=True, blank=True)

  UNIT = 'UT'
  DAY = 'DY'
  MONTH = 'MH'
  YEAR = 'YR'
  USER = 'UR'
  SCHOOL = 'SL'
  TRIAL = 'TL'
  PRICE_UNIT_CHOICES = (
    (UNIT, 'Unit'),   # Use this for one-time price for SW, HW or packs
    (DAY, 'Day'),
    (MONTH, 'Month'),
    (YEAR, 'Year'),
    (USER, 'User'),
    (SCHOOL, 'School'),
    (TRIAL, 'Trial'),
  )
  unit = models.CharField(max_length=2, choices=PRICE_UNIT_CHOICES, default=UNIT)

  def __unicode__(self):
    if self.unit == self.TRIAL:
      return "{0} days trial".format(self.trial_days)
    else:
      return "{0}: ${1}/{2}".format(self.name, self.cost, self.get_unit_display())

class Review(models.Model):
  cs_resource = models.ForeignKey(CSResource)
  user = models.ForeignKey(User)
  description = models.CharField(max_length=400, blank=True)
  rating = models.PositiveSmallIntegerField(null=True,blank=True)
  created_time = models.DateTimeField(auto_now_add=True)
  last_modified_time = models.DateTimeField(auto_now=True)

  class Meta:
    unique_together = ('user', 'cs_resource',)

  def clean(self):
    if rating < 1 or rating > 5:
      raise ValidationError("Rating is outside range:{0}".format(str(stars)))

    if self.description is None and self.rating is None:
      raise ValidationError("Review requires either a description and/or rating")

  def __unicode__(self):
    return "{0} star(s) - {1}".format(self.rating, self.description)

class Platform(models.Model):
  cs_resource = models.ForeignKey(CSResource)

  ACTION = 'AD'
  ADVENTURE = 'WS'
  FANTASY = 'CB'
  DRAMA = 'ME'
  HORROR = 'MC'
  COMEDY = 'LX'
  CRIME = 'IE'
  THRILLER = 'ID'
  ANIMATED = 'JA'
  SERIALS = 'WB'
  OTHER = 'OR'
  PLATFORM_NAME_CHOICES = (
    (ACTION, 'Action'),   # Use this for one-time price for SW, HW or packs
    (ADVENTURE, 'Adventure'),
    (FANTASY, 'Fantasy'),
    (DRAMA, 'Drama'),
    (HORROR, 'Horror'),
    (COMEDY, 'Comedy'),
    (CRIME, 'Crime'),
    (THRILLER, 'Thriller'),
    (ANIMATED, 'Animated'),
    (SERIALS, 'Serials'),
    (OTHER, 'Other'),
  )
  name = models.CharField(max_length=2, choices=PLATFORM_NAME_CHOICES, default=OTHER)
  other_name = models.CharField(max_length=20, blank=True)

  def __unicode__(self):
    return self.other_name if (self.name == self.OTHER) else self.get_name_display()

class Language(models.Model):
  cs_resource = models.ForeignKey(CSResource)

  C = "C"
  C_PLUS_PLUS = "CS"
  JAVA = "JA"
  PYTHON = "PN"
  C_SHARP = "CP"
  JAVASCRIPT = "JT"
  LUA = "LA"
  BLOCKLY = "BY"
  CUSTOM_VISUAL = 'CY'
  TURTLE = 'TE'
  CLOJURE = 'CE'
  COFFEESCRIPT = 'CT'
  ADOBE_FLASH = 'AH'
  OTHER = 'OR'
  LANGUAGE_NAME_CHOICES = (
    (C, 'C'),   # Use this for one-time price for SW, HW or packs
    (C_PLUS_PLUS, 'C++'),
    (JAVA, 'Java'),
    (PYTHON, 'Python'),
    (C_SHARP, 'C#'),
    (JAVASCRIPT, 'JavaScript'),
    (LUA, 'Lua'),
    (BLOCKLY, 'Blockly (google)'),
    (CUSTOM_VISUAL, 'Other visual blocky language'),
    (TURTLE, 'Turtle directions'),
    (JAVA, 'Java'),
    (CLOJURE, 'Clojure'),
    (COFFEESCRIPT, 'CoffeeScript'),
    (ADOBE_FLASH, 'Adobe Flash'),
    (OTHER, 'Other'),
  )
  name = models.CharField(max_length=2, choices=LANGUAGE_NAME_CHOICES, default=OTHER)
  other_name = models.CharField(max_length=20, blank=True)

  def __unicode__(self):
    return self.other_name if (self.name == self.OTHER) else self.get_name_display()