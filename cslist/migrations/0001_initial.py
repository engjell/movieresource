# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='CSResource',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=40)),
                ('url', models.URLField()),
                ('description', models.CharField(max_length=400, blank=True)),
                ('grade_levels', models.CommaSeparatedIntegerField(max_length=28, null=True, blank=True)),
                ('rating_sum', models.IntegerField(default=0)),
                ('rating_count', models.IntegerField(default=0)),
                ('promotional_video_url', models.URLField(null=True, blank=True)),
                ('created_time', models.DateTimeField(auto_now_add=True)),
                ('last_modified_time', models.DateTimeField(auto_now=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Movie Resource',
            },
        ),
        migrations.CreateModel(
            name='Language',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default=b'OR', max_length=2, choices=[(b'C', b'C'), (b'CS', b'C++'), (b'JA', b'Java'), (b'PN', b'Python'), (b'CP', b'C#'), (b'JT', b'JavaScript'), (b'LA', b'Lua'), (b'BY', b'Blockly (google)'), (b'CY', b'Other visual blocky language'), (b'TE', b'Turtle directions'), (b'JA', b'Java'), (b'CE', b'Clojure'), (b'CT', b'CoffeeScript'), (b'AH', b'Adobe Flash'), (b'OR', b'Other')])),
                ('other_name', models.CharField(max_length=20, blank=True)),
                ('cs_resource', models.ForeignKey(to='cslist.CSResource')),
            ],
        ),
        migrations.CreateModel(
            name='Platform',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default=b'OR', max_length=2, choices=[(b'AD', b'Action'), (b'WS', b'Adventure'), (b'CB', b'Fantasy'), (b'ME', b'Drama'), (b'MC', b'Horror'), (b'LX', b'Comedy'), (b'IE', b'Crime'), (b'ID', b'Thriller'), (b'JA', b'Animated'), (b'WB', b'Serials'), (b'OR', b'Other')])),
                ('other_name', models.CharField(max_length=20, blank=True)),
                ('cs_resource', models.ForeignKey(to='cslist.CSResource')),
            ],
        ),
        migrations.CreateModel(
            name='PriceTier',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=40)),
                ('trial_days', models.IntegerField(null=True, blank=True)),
                ('cost', models.FloatField(null=True, blank=True)),
                ('unit', models.CharField(default=b'UT', max_length=2, choices=[(b'UT', b'Unit'), (b'DY', b'Day'), (b'MH', b'Month'), (b'YR', b'Year'), (b'UR', b'User'), (b'SL', b'School'), (b'TL', b'Trial')])),
                ('cs_resource', models.ForeignKey(to='cslist.CSResource')),
            ],
        ),
        migrations.CreateModel(
            name='Review',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('description', models.CharField(max_length=400, blank=True)),
                ('rating', models.PositiveSmallIntegerField(null=True, blank=True)),
                ('created_time', models.DateTimeField(auto_now_add=True)),
                ('last_modified_time', models.DateTimeField(auto_now=True)),
                ('cs_resource', models.ForeignKey(to='cslist.CSResource')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='ScreenShot',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', models.ImageField(upload_to=b'')),
                ('description', models.CharField(max_length=40, null=True, blank=True)),
                ('cs_resource', models.ForeignKey(to='cslist.CSResource')),
            ],
        ),
        migrations.AlterUniqueTogether(
            name='review',
            unique_together=set([('user', 'cs_resource')]),
        ),
    ]
